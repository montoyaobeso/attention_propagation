#!/usr/bin/env python


############################################################
#       DEPENDECIES
############################################################

import sys, os, cv2, csv, time
import numpy as np
from woodingMaps import computeWoodingMap
from homography_tools import *
from shutil import copyfile
import scipy.misc

############################################################
#       FUNCTIONS
############################################################

def loadFixations(fixationsfile):
    '''
    Load fixations file
    '''
    Fixations = []
    with open(fixationsfile) as csvfile:
        spamreader = csv.reader(csvfile, delimiter='\t')
        for row, [x,y] in enumerate(spamreader):
            Fixations.append([float(x),float(y)])
    Fixations = np.array([Fixations])
    return Fixations

def normalizeImageSize(image):
    '''
    Return normalized image and original aspect ratio for fixations resizing
    '''
    Ih, Iw = image.shape[:2]
    if Iw > Ih:
        Ratio = 512 / float(Iw)
        I = cv2.resize(image, (int(Iw*Ratio), int(Ih*Ratio)), interpolation = cv2.INTER_CUBIC)
    if Iw < Ih:
        Ratio = 512 / float(Ih)
        I = cv2.resize(image, (int(Iw*Ratio), int(Ih*Ratio)), interpolation = cv2.INTER_CUBIC)
    if Iw == Ih:
        Ratio = 512 / float(Iw)
        I = cv2.resize(image, (int(Iw*Ratio), int(Ih*Ratio)), interpolation = cv2.INTER_CUBIC)
    return I, Ratio

def dropKeyPoints(kp, des, thr):
    '''
    This function drop weak keypoints
    kp:     keypoints vector
    des:    descriptors vector
    thr:    threshold
    '''
    init_kp = len(kp)
    kp1bk = []
    des1bk = []
    stats = []
    for ind in range(len(kp)):
        if float(kp[ind].response) > thr:
            kp1bk.append(kp[ind])
            stats.append(kp[ind].response)
            des1bk.append(des[ind])
    kp = np.array(kp1bk)
    des = np.array(des1bk)
    stats = np.array(stats)
    return kp, des

def drawText(img, pos, text):
    '''
    Draw some text on the input image
    '''
    font                   = cv2.FONT_HERSHEY_SIMPLEX
    bottomLeftCornerOfText = pos
    fontScale              = 1
    fontColor              = (255,255,255)
    lineType               = 2

    cv2.putText(img, text,
        bottomLeftCornerOfText,
        font,
        fontScale,
        fontColor,
        lineType)


############################################################
#                       MAIN CODE
############################################################

# Set reference image and fixations path
ReferenceImage      = './reference_data/Colonial_BasilicaDenuestrasenoraDeGuanajuato_N_1.png'
ReferenceFixations  = './reference_data/Colonial_BasilicaDenuestrasenoraDeGuanajuato_GazeFix_N_1.txt'

# Set target image path
TargetImage = './target_data/Colonial_BasilicaDenuestrasenoraDeGuanajuato_N_2.jpg'

RATIO       = 0.7
KPMIN       = 10
RANSAC      = 5.0
ANGLE_TH    = 20


# LOAD GRAYSCALE IMAGES
img1 = cv2.imread(ReferenceImage, 0)
img1, RatioReference = normalizeImageSize(img1)
img2 = cv2.imread(TargetImage, 0)
img2, RatioTarget = normalizeImageSize(img2)

# LOAD COLOR IMAGES
img1c = cv2.imread(ReferenceImage, 1) #  in color
img1c, RatioReference = normalizeImageSize(img1c)
img2c = cv2.imread(TargetImage, 1) #  in color
img2c, RatioTarget = normalizeImageSize(img2c)

# find the keypoints and descriptors with SIFT
sift = cv2.xfeatures2d.SIFT_create()
kp1, des1 = sift.detectAndCompute(img1,None)
kp1, des1 = dropKeyPoints(kp1, des1, 0.03) # Drop weak keypoints
kp2, des2 = sift.detectAndCompute(img2,None)
kp2, des2 = dropKeyPoints(kp2, des2, 0.03) # Drop weak keypoints

# FLANN parameters
FLANN_INDEX_KDTREE = 0
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
search_params = dict(checks = 50)
flann = cv2.FlannBasedMatcher(index_params, search_params)
matches = flann.knnMatch(np.asarray(des1,np.float32),np.asarray(des2,np.float32), k=2)

RATIOS = np.linspace(0.7, 1.0, 30)
for ratio in RATIOS:
    good = []
    for m,n in matches:
        if m.distance < ratio*n.distance:
            good.append(m)

    if len(good) >= KPMIN:
        src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, RANSAC)
        if M is None:
            break
        h, w = img1.shape
        pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)

        try:
            dst = cv2.perspectiveTransform(pts,M)
        except (RuntimeError, TypeError, NameError):
            break

        # Perform geometry test
        SquareTest = testHomographyAngle(dst, ANGLE_TH)

        if SquareTest:

            Fixations = loadFixations(ReferenceFixations)
            targetFixations = cv2.perspectiveTransform(Fixations * RatioReference, M)
            Ih, Iw = img2c.shape[:2]
            CS = computeWoodingMap(targetFixations[0], Iw, Ih)

            CS = cv2.applyColorMap(np.uint8(255*CS), cv2.COLORMAP_JET)
            CS = cv2.addWeighted(CS, 0.7, img2c, 0.4, 0)

            img2copy = cv2.polylines(img2c.copy(),[np.int32(dst)],True,(0,255,0),3, cv2.LINE_AA)

            matchesMask = mask.ravel().tolist()
            draw_params = dict(matchColor = (0,0,255), # draw keypoints in blue color
                            singlePointColor = (255,0,0), # draw matching lines in red
                            matchesMask = matchesMask, # draw only good matches for homography
                            flags = 0)

            img3 = cv2.drawMatches(img1c,kp1,img2copy,kp2,good,None,**draw_params)

            h1, w1 = img3.shape[:2]
            h2, w2 = CS.shape[:2]

            #create empty matrix to concatenate images
            vis = np.zeros((max(h1, h2), w1+w2,3), np.uint8)
            #combine 2 images
            vis[:h1, :w1,:3] = img3
            vis[:h2, w1:w1+w2,:3] = CS

            drawText(vis, (10,40),'Press space to continue...') #write current frame
            drawText(vis, (10,80),'Ratio: {:.2f}'.format(ratio)) #write current frame

            cv2.imshow('Matches', vis)
            print "For ratio {}, {} matches found... The homography is {} for RANSAC {}...\n".format(ratio, len(good), SquareTest,RANSAC)


        else:

            img2copy = cv2.polylines(img2c.copy(),[np.int32(dst)],True,(0,255,0),3, cv2.LINE_AA)
            draw_params = dict(matchColor = (0,0,255), # draw keypoints in blue color
                            singlePointColor = (255,0,0), # draw matching lines in red
                            matchesMask = None, # draw only good matches for homography
                            flags = 0)
            img3 = cv2.drawMatches(img1c,kp1,img2copy,kp2,good,None,**draw_params)

            drawText(img3, (10,40),'Press space to continue...') #write current frame
            drawText(img3, (10,80),'Ratio: {:.2f}'.format(ratio)) #write current frame
            cv2.imshow('Matches', img3)

        if cv2.waitKey(0) & 0xFF == ord('q'):
            break
