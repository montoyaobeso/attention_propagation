#!/usr/bin/env python

# ##################################################################
# #                         DPENDENCES                             #
# ##################################################################
import csv, shutil, os, time, Image
import numpy as np
from scipy.spatial import distance as dist
from matplotlib.pyplot import *
import scipy.misc
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

def transparent_cmap(cmap, N=255):
    "Copy colormap and set alpha values"
    mycmap = cmap
    mycmap._init()
    mycmap._lut[:,-1] = np.linspace(0.2, 1.0, N+4)
    return mycmap

def computeWoodingMap(Fixations, Iw, Ih):
    Mw, Mh = 1920.0, 1200.0
    # Ratio if the image fits the monitor vertically
    FitRatioW = Mw / Iw
    # Ratio if the image fits the monitor horizontally
    FitRatioH = Mh / Ih

    # Check a Valid Ratio to fit the screen
    if Iw*FitRatioW <= Mw and Ih*FitRatioW <= Mh:
        FitRatio = FitRatioW
        # print "--HORIZONTAL FIT--"
        # Screen Image Dimensions
        SIh = Ih*FitRatio
        SIw = Iw*FitRatio
        # Shift
        ShiftH = (Mh - SIh) / 2
        ShiftW = 0
        FitImageRatio = Iw / SIw
    else:
        # print "--VERTICAL FIT--"
        FitRatio = FitRatioH
        # Screen Image Dimensions
        SIh = Ih*FitRatio
        SIw = Iw*FitRatio
        # Shift
        ShiftW = (Mw - SIw) / 2
        ShiftH = 0
        FitImageRatio = Ih / SIh

    D = 900 * FitImageRatio # mm (~3*H)
    Mh_mm = 325.00 * FitImageRatio # Display heigth in mm
    Mh = 1200.00 * FitImageRatio # Display heigth in px
    alpha = 2 # Vision angle
    R = Mh / Mh_mm # Display resolution per mm
    sig_mm = D * np.tan(np.radians(alpha))
    sig = (R * sig_mm)

    Ssubj = np.zeros((Ih,Iw), dtype=float)
    A = 1

    Xi, Yi = np.mgrid[0:Ih, 0:Iw] # Grid to vectorize Gaussian for each measure

    for i,M in enumerate(Fixations):
        m = (int(M[0]), int(M[1]))
        Ssubj += A*np.exp( -( ( ((Yi-m[0])**2) / (2*(sig**2)) ) + ( ((Xi-m[1])**2) / (2*(sig**2)) ) ) )

    Ssubj = Ssubj / np.max(Ssubj) # Normalize

    return Ssubj
