# -*- coding: utf-8 -*-
"""
Created on Tue Apr 24 21:13:03 2018

@author: Abraham
"""
import os
import numpy as np
import cv2
import math

def euclideanDistance(P1,P2):
    x1, y1 = P1
    x2, y2 = P2
    d = math.sqrt((x1-x2)**2 + (y1-y2)**2)
    return d


def testRange(a,t):
    if int(a) >= 90-t and int(a) <= 90+t:
        return True
    else:
        return False

def getAngle(T):
    P=T[0]
    Q=T[1]
    R=T[2]
    r = euclideanDistance(P,Q)
    p = euclideanDistance(Q,R)
    q = euclideanDistance(R,P)

    # aP = np.rad2deg(np.arccos(np.minimum(1,(r**2 + q**2 - p**2)/float(2*r*q))))
    if r > 0 and p > 0 and q > 0:
        aQ = np.rad2deg(np.arccos(np.minimum(1,(p**2 + r**2 - q**2)/float(2*p*r))))
        # aR = np.rad2deg(np.arccos(np.minimum(1,(q**2 + p**2 - r**2)/float(2*p*q))))
        if math.isnan(aQ):
            aQ = 0.
        return aQ
    else:
        return 0.

def testHomographyAngle(dst,ang_tr):
    P = dst[0][0]
    Q = dst[1][0]
    R = dst[2][0]
    S = dst[3][0]

    px,py = P
    qx,qy = Q
    rx,ry = R
    sx,sy = S

    T1 = [[px,py],[qx,qy],[rx,ry]]
    T2 = [[qx,qy],[rx,ry],[sx,sy]]
    T3 = [[rx,ry],[sx,sy],[px,py]]
    T4 = [[sx,sy],[px,py],[qx,qy]]

    a1 = getAngle(T1)
    ta1 = testRange(a1,ang_tr)
    a2 = getAngle(T2)
    ta2 = testRange(a2,ang_tr)
    a3 = getAngle(T3)
    ta3 = testRange(a3,ang_tr)
    a4 = getAngle(T4)
    ta4 = testRange(a4,ang_tr)
    tAll = int(np.round(a1+a2+a3+a4)) == 360

    if tAll and ta1 and ta2 and ta3 and ta4:
        return True
    else:
        return False
